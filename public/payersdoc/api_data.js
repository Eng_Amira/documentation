define({ "api": [
  {
    "type": "delete",
<<<<<<< HEAD
    "url": "/items/:id",
    "title": "Delete an Item",
=======
    "url": "/item/:id",
    "title": "delete Item",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
    "version": "0.3.0",
    "name": "DeleteItem",
    "group": "Item",
    "permission": [
      {
<<<<<<< HEAD
        "name": "admin"
      }
    ],
    "description": "<p>Delete an item.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/items/1",
        "type": "json"
      }
    ],
=======
        "name": "none"
      }
    ],
    "description": "<p>used to delete existing item</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>ID of the Item.</p>"
          }
        ]
      }
    },
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
    "success": {
      "examples": [
        {
          "title": "Response (example):",
<<<<<<< HEAD
          "content": " HTTP/ 200 Success\n {\n   \"success\": \"200 ok\"\n}",
=======
          "content": " HTTP/1.1 200 OK\n{\n   \"id\": \"id\"\n     }",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
          "type": "json"
        }
      ]
    },
    "error": {
<<<<<<< HEAD
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated Admins can delete the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ItemNotFound",
            "description": "<p>The <code>id</code> of the Item was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
=======
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Not Authenticated\n{\n   \"error\": \"NoDeleteRight\"\n}",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/items_controller.rb",
    "groupTitle": "Item"
  },
  {
    "type": "get",
<<<<<<< HEAD
    "url": "/items/:id",
    "title": "Read data of an Item",
=======
    "url": "/items",
    "title": "List all Items",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
    "version": "0.3.0",
    "name": "GetItem",
    "group": "Item",
    "permission": [
      {
<<<<<<< HEAD
        "name": "admin"
      }
    ],
    "description": "<p>get details of an item.</p>",
=======
        "name": "none"
      }
    ],
    "description": "<p>to get data of all items.</p>",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
<<<<<<< HEAD
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>The Item-ID.</p>"
=======
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>The Items-ID.</p>"
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
<<<<<<< HEAD
        "content": "curl -i http://localhost/items/1",
=======
        "content": "curl -i http://localhost/items",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
<<<<<<< HEAD
            "type": "Integer",
=======
            "type": "integer",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
            "optional": false,
            "field": "id",
            "description": "<p>The Items-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
<<<<<<< HEAD
            "description": "<p>The Item title</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
=======
            "description": "<p>title.</p>"
          },
          {
            "group": "Success 200",
            "type": "datetime",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the item.</p>"
          },
          {
            "group": "Success 200",
<<<<<<< HEAD
            "type": "Datetime",
=======
            "type": "datetime",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the item.</p>"
          }
        ]
<<<<<<< HEAD
      }
=======
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 200 OK\n{\n   \"id\": \"id\"\n   \"title\": \"title\"\n   \"created_at\": \"created_at\"\n   \"updated_at\": \"updated_at\"\n}",
          "type": "json"
        }
      ]
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated Admins can access the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ItemNotFound",
            "description": "<p>The <code>id</code> of the Item was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
<<<<<<< HEAD
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
=======
          "content": " HTTP/1.1 401 Not Authenticated\n{\n   \"error\": \"NoAccessRight\"\n}",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/items_controller.rb",
    "groupTitle": "Item"
  },
  {
    "type": "get",
<<<<<<< HEAD
    "url": "/items/",
    "title": "Read all Items",
=======
    "url": "/item/:id",
    "title": "Read data of an Item",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
    "version": "0.3.0",
    "name": "GetItem",
    "group": "Item",
    "permission": [
      {
<<<<<<< HEAD
        "name": "admin"
      }
    ],
    "description": "<p>get details of all items.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/items",
=======
        "name": "none"
      }
    ],
    "description": "<p>to get data of specific item.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>The Items-ID.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/item/4711",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
<<<<<<< HEAD
            "type": "Integer",
=======
            "type": "String",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
            "optional": false,
            "field": "id",
            "description": "<p>The Items-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
<<<<<<< HEAD
            "description": "<p>The Item title</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the item.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the item.</p>"
=======
            "description": "<p>title.</p>"
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated Admins can access the data.</p>"
<<<<<<< HEAD
=======
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ItemNotFound",
            "description": "<p>The <code>id</code> of the Item was not found.</p>"
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
<<<<<<< HEAD
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
=======
          "content": " HTTP/1.1 401 Not Authenticated\n{\n   \"error\": \"NoAccessRight\"\n}",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/items_controller.rb",
    "groupTitle": "Item"
  },
  {
<<<<<<< HEAD
    "type": "put",
    "url": "/items/:id/edit",
    "title": "Update data of an Item",
    "version": "0.3.0",
    "name": "PutItem",
    "group": "Item",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "description": "<p>update details of an item.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>The Item-title.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/items/1/edit",
        "type": "json"
      }
    ],
=======
    "type": "post",
    "url": "/items/new",
    "title": "Create a new Item",
    "version": "0.3.0",
    "name": "PostItem",
    "group": "Item",
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>used to create new item.</p>",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
<<<<<<< HEAD
            "description": "<p>The Items-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>The Item title</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
=======
            "description": "<p>The new Items-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "datetime",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the item.</p>"
          },
          {
            "group": "Success 200",
<<<<<<< HEAD
            "type": "Datetime",
=======
            "type": "datetime",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the item.</p>"
          }
        ]
<<<<<<< HEAD
=======
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 200 OK\n{\n   \"id\": \"id\"\n   \"title\": \"title\"\n   \"created_at\": \"created_at\"\n   \"updated_at\": \"updated_at\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>title of the Item.</p>"
          }
        ]
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
<<<<<<< HEAD
            "description": "<p>Only authenticated Admins can update the data.</p>"
=======
            "description": "<p>Only authenticated Admins can access the data.</p>"
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ItemNotFound",
            "description": "<p>The <code>id</code> of the Item was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
<<<<<<< HEAD
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
=======
          "content": " HTTP/1.1 401 Not Authenticated\n{\n   \"error\": \"NoUpdateRight\"\n}",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/items_controller.rb",
    "groupTitle": "Item"
  },
  {
<<<<<<< HEAD
    "type": "post",
    "url": "/items/",
    "title": "Create new Item",
    "version": "0.3.0",
    "name": "postItem",
    "group": "Item",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "description": "<p>create new item.</p>",
=======
    "type": "put",
    "url": "/item/:id/edit",
    "title": "update Item",
    "version": "0.3.0",
    "name": "PutItem",
    "group": "Item",
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>used to update data for existing item</p>",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
<<<<<<< HEAD
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>The Item title</p>"
=======
            "type": "Id",
            "optional": false,
            "field": "ID",
            "description": "<p>of the Item.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Name of the Item.</p>"
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
          }
        ]
      }
    },
<<<<<<< HEAD
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/items/",
        "type": "json"
      }
    ],
=======
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
<<<<<<< HEAD
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>The Items-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>The Item title</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
=======
            "type": "datetime",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the item.</p>"
          },
          {
            "group": "Success 200",
<<<<<<< HEAD
            "type": "Datetime",
=======
            "type": "datetime",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the item.</p>"
          }
        ]
<<<<<<< HEAD
      }
=======
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 200 OK\n{\n   \"id\": \"id\"\n   \"title\": \"title\"\n   \"created_at\": \"created_at\"\n   \"updated_at\": \"updated_at\"\n}",
          "type": "json"
        }
      ]
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
<<<<<<< HEAD
            "description": "<p>Only authenticated Admins can createcitem.</p>"
=======
            "description": "<p>Only authenticated Admins can access the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ItemNotFound",
            "description": "<p>The <code>id</code> of the Item was not found.</p>"
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
<<<<<<< HEAD
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
=======
          "content": " HTTP/1.1 401 Not Authenticated\n{\n   \"error\": \"NoUpdateRight\"\n}",
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/items_controller.rb",
    "groupTitle": "Item"
  }
] });
