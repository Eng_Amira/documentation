<<<<<<< HEAD
#=begin
  # @api {get} /items/:id Read data of an Item
  # @apiVersion 0.3.0
  # @apiName GetItem
  # @apiGroup Item
  # @apiPermission admin
  #
  # @apiDescription get details of an item.
  #
  # @apiParam {Integer} id The Item-ID.
  #
  # @apiExample Example usage:
  # curl -i http://localhost/items/1
  #
  # @apiSuccess {Integer}   id            The Items-ID.
  # @apiSuccess {String}     title         The Item title
  # @apiSuccess {Datetime}   created_at    date of creating the item.
  # @apiSuccess {Datetime}   updated_at    date of updating the item.
  #
  # @apiError NoAccessRight Only authenticated Admins can access the data.
  # @apiError ItemNotFound   The <code>id</code> of the Item was not found.
  #
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Not Authenticated
  #     {
  #       "error": "NoAccessRight"
  #    }
#=end
#=begin
  # @api {put} /items/:id/edit Update data of an Item
  # @apiVersion 0.3.0
  # @apiName PutItem
  # @apiGroup Item
  # @apiPermission admin
  #
  # @apiDescription update details of an item.
  #
  # @apiParam {String} title The Item-title.
  #
  # @apiExample Example usage:
  # curl -i http://localhost/items/1/edit
  #
  # @apiSuccess {Integer}   id            The Items-ID.
  # @apiSuccess {String}     title         The Item title
  # @apiSuccess {Datetime}   created_at    date of creating the item.
  # @apiSuccess {Datetime}   updated_at    date of updating the item.
  #
  # @apiError NoAccessRight Only authenticated Admins can update the data.
  # @apiError ItemNotFound   The <code>id</code> of the Item was not found.
  #
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Not Authenticated
  #     {
  #       "error": "NoAccessRight"
  #    }
#=end
#=begin
  # @api {delete} /items/:id Delete an Item
  # @apiVersion 0.3.0
  # @apiName DeleteItem
  # @apiGroup Item
  # @apiPermission admin
  #
  # @apiDescription Delete an item.
  #
  # @apiExample Example usage:
  # curl -i http://localhost/items/1
  #
  # @apiSuccessExample Response (example):
  #     HTTP/ 200 Success
  #     {
  #       "success": "200 ok"
  #    }
  # @apiError NoAccessRight Only authenticated Admins can delete the data.
  # @apiError ItemNotFound   The <code>id</code> of the Item was not found.
  #
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Not Authenticated
  #     {
  #       "error": "NoAccessRight"
  #    }
#=end
#=begin
  # @api {post} /items/ Create new Item
  # @apiVersion 0.3.0
  # @apiName postItem
  # @apiGroup Item
  # @apiPermission admin
  #
  # @apiDescription create new item.
  #
  # @apiParam {String}     title         The Item title
  #
  # @apiExample Example usage:
  # curl -i http://localhost/items/
  #
  # @apiSuccess {Integer}   id            The Items-ID.
  # @apiSuccess {String}     title         The Item title
  # @apiSuccess {Datetime}   created_at    date of creating the item.
  # @apiSuccess {Datetime}   updated_at    date of updating the item.
  #
  # @apiError NoAccessRight Only authenticated Admins can createcitem.
  #
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Not Authenticated
  #     {
  #       "error": "NoAccessRight"
  #    }
#=end
#=begin
  # @api {get} /items/ Read all Items
  # @apiVersion 0.3.0
  # @apiName GetItem
  # @apiGroup Item
  # @apiPermission admin
  #
  # @apiDescription get details of all items.
  #
  #
  # @apiExample Example usage:
  # curl -i http://localhost/items
  #
  # @apiSuccess {Integer}   id            The Items-ID.
  # @apiSuccess {String}     title         The Item title
  # @apiSuccess {Datetime}   created_at    date of creating the item.
  # @apiSuccess {Datetime}   updated_at    date of updating the item.
  #
  # @apiError NoAccessRight Only authenticated Admins can access the data.
  #
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Not Authenticated
  #     {
  #       "error": "NoAccessRight"
  #    }
#=end
=======
=begin
  # @api {get} /items List all Items
  # @apiVersion 0.3.0
  # @apiName GetItem
  # @apiGroup Item
  # @apiPermission none
 
  # @apiDescription to get data of all items.
 
  # @apiParam {String} id The Items-ID.
  # @apiExample Example usage:
  # curl -i http://localhost/items
 
  # @apiSuccess {integer}   id            The Items-ID.
  # @apiSuccess {String}   title         title.
  # @apiSuccess {datetime}   created_at         date of creating the item.
  # @apiSuccess {datetime}   updated_at         date of updating the item.

 
  # @apiError NoAccessRight Only authenticated Admins can access the data.
  # @apiError ItemNotFound   The <code>id</code> of the Item was not found.

  # @apiSuccessExample Response (example):
  #     HTTP/1.1 200 OK
  #    {
  #       "id": "id"
  #       "title": "title"
  #       "created_at": "created_at"
  #       "updated_at": "updated_at"
  #    }
 
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Not Authenticated
  #    {
  #       "error": "NoAccessRight"
  #    }
=end


=begin
  # @api {get} /item/:id Read data of an Item
  # @apiVersion 0.3.0
  # @apiName GetItem
  # @apiGroup Item
  # @apiPermission none
 
 # @apiDescription to get data of specific item.
 
 # @apiParam {String} id The Items-ID.
 # @apiExample Example usage:
 # curl -i http://localhost/item/4711
 
 # @apiSuccess {String}   id            The Items-ID.
 # @apiSuccess {String}   title         title.

 
 # @apiError NoAccessRight Only authenticated Admins can access the data.
 # @apiError ItemNotFound   The <code>id</code> of the Item was not found.
 
 # @apiErrorExample Response (example):
 #     HTTP/1.1 401 Not Authenticated
  #    {
 #       "error": "NoAccessRight"
 #    }
=end



=begin
  # @api {post} /items/new Create a new Item
  # @apiVersion 0.3.0
  # @apiName PostItem
  # @apiGroup Item
  # @apiPermission none
 
  # @apiDescription used to create new item.
  
  # @apiSuccess {Integer} id     The new Items-ID.
  # @apiParam {String} title title of the Item. 
  # @apiSuccess {datetime}   created_at         date of creating the item.
  # @apiSuccess {datetime}   updated_at         date of updating the item.

  # @apiError NoAccessRight Only authenticated Admins can access the data.
  # @apiError ItemNotFound   The <code>id</code> of the Item was not found.

  # @apiSuccessExample Response (example):
  #     HTTP/1.1 200 OK
  #    {
  #       "id": "id"
  #       "title": "title"
  #       "created_at": "created_at"
  #       "updated_at": "updated_at"
  #    }
 
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Not Authenticated
  #    {
  #       "error": "NoUpdateRight"
  #    }
  
 
=end

=begin
  # @api {put} /item/:id/edit update Item
  # @apiVersion 0.3.0
  # @apiName PutItem
  # @apiGroup Item
  # @apiPermission none
 
  # @apiDescription used to update data for existing item
  # @apiParam {Id} ID of the Item.
  # @apiParam {String} title Name of the Item.
  # @apiSuccess {datetime}   created_at         date of creating the item.
  # @apiSuccess {datetime}   updated_at         date of updating the item.

  # @apiError NoAccessRight Only authenticated Admins can access the data.
  # @apiError ItemNotFound   The <code>id</code> of the Item was not found.

  # @apiSuccessExample Response (example):
  #     HTTP/1.1 200 OK
  #    {
  #       "id": "id"
  #       "title": "title"
  #       "created_at": "created_at"
  #       "updated_at": "updated_at"
  #    }
 
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Not Authenticated
  #    {
  #       "error": "NoUpdateRight"
  #    }
=end

=begin
  # @api {delete} /item/:id delete Item
  # @apiVersion 0.3.0
  # @apiName DeleteItem
  # @apiGroup Item
  # @apiPermission none
 
  # @apiDescription used to delete existing item
  # @apiParam {Integer} id ID of the Item.

  # @apiSuccessExample Response (example):
  #     HTTP/1.1 200 OK
  #    {
  #       "id": "id"
  #         }
 
  
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Not Authenticated
  #    {
  #       "error": "NoDeleteRight"
  #    }
=end


>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :update, :destroy]

  # GET /items
  def index
    @items = Item.all

    render json: @items
  end

  # GET /items/1
<<<<<<< HEAD
  
=======
>>>>>>> 2efed3d6b857eaab74ac3bbc9205096b1f6a70bc
  def show
    render json: @item
  end

  # POST /items
  def create
    @item = Item.new(item_params)

    if @item.save
      render json: @item, status: :created, location: @item
    else
      render json: @item.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /items/1
  def update
    if @item.update(item_params)
      render json: @item
    else
      render json: @item.errors, status: :unprocessable_entity
    end
  end

  # DELETE /items/1
  def destroy
    @item.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def item_params
      params.require(:item).permit(:title)
    end
end
